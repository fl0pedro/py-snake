#!/usr/bin/python3

from Game import Game
from Display import Display

import sys
import atexit
from curses import KEY_RIGHT, KEY_LEFT, KEY_DOWN, KEY_UP

START = (7, 7)
SIZE = 16
BOARD_SIZE = (SIZE, SIZE)
DELAY = 0.1

game = Game(BOARD_SIZE, START)
display = Display(BOARD_SIZE, DELAY)

dir_dict = {
        KEY_UP    : 'n',
        KEY_RIGHT : 'e',
        KEY_DOWN  : 's',
        KEY_LEFT  : 'w'
}
keys = list(dir_dict.keys())

pause = False
dir_ = 'x'

snake, food = game.get_board()

while not game.check_game_end():
    key = display.key_pressed()
    if key == ord('q'):
        break
    elif pause and key != -1:
        pause = False
    elif key == ord('p'):
        pause = True
    
    if pause: continue
        
    if key in keys: game.dir = dir_dict[key]
    
    game.move()
    
    display.draw(snake.queue, food.coords)
    if SIZE > 4: 
        display.scores(
                game.points, 
                game.frames, 
                game.turns
        )

display.end()
if game.points == 2**(SIZE*2): victory = True
else: victory = False

print('victory: ', str(victory))
print('points: ', game.points)
print('frames: ', game.frames)
print('turns: ', game.turns)
