#!/usr/bin/python3

import random

class Game:
    
    class Snake:
    
        def __init__(self, head):
            self.head = head
            self.dir = (0, 0)
            self.queue = [head]

        def sum_tuples(self, a, b):
            return tuple(map(sum,zip(a, b)))

        def move(self):
            self.head = self.sum_tuples(
                self.head, self.dir
            )
            
            self.queue.append(self.head)
            if not self.grow(): 
                self.queue.pop(0)
    
        def grow(self):
            return self.head == self.food

    class Food:
        def __init__(self, board_size):
            self.board_size = board_size
            self.generate_full()

        def generate_full(self):
            x, y = self.board_size
            self.coords = (
                    random.randint(1, x - 1),
                    random.randint(1, y - 1)
            )

        def generate(self, board): # in open space
            if len(board): 
                self.coords = board[
                    random.randint(0, len(board) - 1)
                ]
                return True
            else: return False

    def __init__(self, board_size, start = (1, 1)):
        self.board_size = board_size
        self.game_end = False
        self.snake = self.Snake(start)
        self.food = self.Food(board_size)

        self.points = 0
        self.frames = 0
        self.turns = 0
        self.dir = 'x'

        self.snake.food = self.food.coords

        self.dirs_dict = {
            'n': (0 , -1),
            'e': (1 ,  0),
            's': (0 ,  1),
            'w': (-1,  0),
            'x': (0 ,  0)
        }

        x, y = board_size
        
        self.board = [[(i,j) for j in range(y)] for i in range(x)]
	
    def check_game_end(self):
        x_max , y_max = self.board_size
        x, y = self.snake.head

        if (x, y) in self.snake.queue[:-1] \
            or x >= x_max or y >= y_max \
            or x < 0 or y < 0:
                self.game_end = True 
        return self.game_end
    
    def move(self):

        dir_ = self.dir

        # --------- direction ---------
        if dir_ in self.dirs_dict:
            new_dir = self.dirs_dict[dir_]
            op_new_dir = tuple(map(lambda a: a * -1, new_dir))
            if new_dir != self.snake.dir \
                and op_new_dir != self.snake.dir:
                    self.snake.dir = new_dir
                    self.turns += 1
        
        # --------- grow / eat ---------
        if self.snake.grow():
            valid_area = [i for i in sum(self.board, []) if i not in self.snake.queue]
            self.game_end = not self.food.generate(valid_area)
            self.snake.food = self.food.coords
            self.points += 1

        # --------- move ----------
        self.snake.move()
        self.frames += 1

    def get_board(self):
        return (self.snake, self.food)