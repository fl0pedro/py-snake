#!/usr/bin/python3

# Make this more efficient by only re drawing the text and snake.

import curses
import curses.textpad
import time

class Display:

    def __init__(self, board_size, delay):
        self.board_size = board_size
        self.delay = delay

        x, y = board_size
        x = (x + 2) * 2 - 2
        y += 2
        
        curses.initscr()
        self.window = curses.newwin(y + 3, x, 0, 0)
        self.window.timeout(10)
        self.window.keypad(1)
        curses.noecho()
        curses.curs_set(0)
        #self.window.border(0)
        #curses.textpad.rectangle(self.window, 0, 0, x - 3, y - 3)
        curses.start_color()
        curses.use_default_colors
        curses.init_pair(1, 1, 0)
        curses.init_pair(2, 2, 0)

    def draw_snake(self, body):
        for coords in body:
            x, y = coords
            x = (x + 1) * 2 - 1
            y += 1
            
            if not x <= 0 or x >= (self.board_size[0] + 2) * 2:
                c = curses.color_pair(2)
                self.window.addstr(y, x, '██', c)

    def draw_food(self, coords):
        x, y = coords
        x = (x + 1) * 2 - 1
        y += 1
        self.window.addstr(y, x, '██', curses.color_pair(1))

    def draw(self, snake, apple):
        x, y = self.board_size
        self.seconds = time.time()
        self.window.clear()
        #self.window.border(0)
        curses.textpad.rectangle(self.window, 0, 0, x+1, y*2+1)
        self.draw_food(apple)
        self.draw_snake(snake)
        time.sleep(self.delay)
        self.seconds -= time.time()
        self.seconds *= -1

    def scores(self, points, frames, turns):
        s = 1/self.seconds
        p = ''
        if int(s/1000): 
            s /= 1000
            p += 'k'
        p += ' '
        fps = ' fps: ' + str(round(s, 2)) + p
        x, y = self.board_size
        x += 2; y += 1
        self.window.addstr(y, 1, fps)
        self.window.addstr(y + 1, 2, 'points: ' + str(points))
        self.window.addstr(y + 2, 2, 'frames: ' + str(frames))
        self.window.addstr(y + 3, 2, 'turns: ' + str(turns))

    def print(self, string):
        self.window.addstr(0, 2, string)

    def key_pressed(self):
        return self.window.getch()
        
    def end(self):
        time.sleep(self.delay*10)
        curses.endwin()
